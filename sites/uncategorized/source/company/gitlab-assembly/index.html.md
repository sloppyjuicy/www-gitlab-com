---
layout: handbook-page-toc
title: "GitLab Assembly"
description: "At the start of every Fiscal Year (beginning of February), the E-Group hosts a GitLab Assembly meeting"
---  

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Assembly

At the start of every Fiscal Year (beginning of February), the [E-Group](/company/team/structure/#e-group) hosts a GitLab Assembly meeting. The call is 50 minutes long and takes place over video. It is scheduled at two separate times so team members in all time zones have the opportunity to attend.

The goal of the Assembly is to discuss the strategy and plans for the next fiscal year. Each part of the business will be included.

The call concludes with an AMA with all of E-Group. Team members can ask questions of any particular executive. The AMA doc will be circulated early so that team members can submit questions.

This is a [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/) initiative. The meeting is scheduled by the [EBA to the CEO](/job-families/people-ops/executive-business-administrator/). The call is recorded and posted to Google Drive. The recording is internal because we talk about financial metrics which are not public.

### FY22 GitLab Assembly

The FY22 GitLab Assembly (previously called Fiscal Year Kickoff) took place February 18, 2021 at 8:00am and 5:00pm Pacific Time. It was attended live by approximately 800 team members, with remaining team members encouraged to watch the recording asynchronously.

The event was internal because it featured financial metrics and other information which was not public. It was recorded, and the recording is available [in Drive](https://drive.google.com/file/d/1V_yohghvDpKQf4sXlNGe_6LgscVjxfXo/view?usp=sharing).

This event was approximately 40 minutes long and was hosted on Hopin. The agenda was as follows:
1. Welcome
1. FY21/22 Highlights - [CEO](/job-families/chief-executive-officer/) & [CRO](job-families/sales/chief-revenue-officer/)
1. GitLab is in Rarefied Air - [CPO](/job-families/people-ops/chief-people-officer/) & [CFO](/job-families/finance/chief-financial-officer/)
1. Marketing Vision - [CMO](/job-families/marketing/chief-marketing-officer/)
1. Product Themes - [Chief Product Officer](/job-families/product/chief-product-officer/)
1. Product Vision - [VP Product Management](/job-families/product/product-management-leadership/#vp-of-product-management)
1. Engineering Vision - [CTO](/job-families/engineering/engineering-management/#chief-technology-officer)
1. FY22 Vision - CEO & CRO
1. Closing and randomized coffee chats

Based on [feedback from the FY21 kickoff](#improvements-to-be-made) and in the spirit of iteration, there were some changes to the format. Changes included:
* Adding a produced video, created mostly using highlights from [Sales Kickoff](/handbook/sales/training/SKO/) presentations, to share the most important information. This mostly replaced the previous format of sequential 5-minute live updates.
* Adding a second time to be inclusive of all time zones.
* During each event, attendees watched the video update live (no requirement to complete any actions before the meeting). The video is followed by an AMA with the executive team.
* More focus on the big picture, inspiration, and team alignment.
* Inclusivity: the video was captioned and industry terms, acronyms, or jargon which aren’t well-known were defined.


### FY21 Kickoff

The FY21 Kickoff call was the first of its sort. 
The recording is on [GitLab Unfiltered](https://youtu.be/XlYsmj5fCcI) (GitLab Internal).
The recording is internal because we talk about financial metrics which are not public. 
The slides can be found in the Drive under the title "2020-02-06 FY21 Kickoff".

We collected feedback in [cos-team#15](https://gitlab.com/gitlab-com/cos-team/issues/15).

The structure was roughly as follows:
* 5 minutes: Intro, Welcome, Overview
* 5 minutes: CEO Overview- Accomplishments, Values, Upcoming, Key Focus
* 5 minutes: Sales
* 5 minutes: Product
* 5 minutes: Product Strategy
* 5 minutes: Engineering
* 5 minutes: People
* 5 minutes: Marketing (Standup break)
* 5 minutes: Finance
* 5 minutes: Legal
* Remaining time: AMA with all execs

Feedback on the event was overall positive, with over 600 team members attending.

![fy21_kickoff_attendance_stats](fy21_kickoff_attendance_stats.png)

#### Improvements to be made
There were a number of problems that presented themselves. Below is a list of the problems and some proposed solutions to be solved for the FY22 Kickoff

| Problem | Possible Solution |
|---------|-------------------|
| People were locked out of the doc. |* We have 1-5 people designated as "typers" and everyone else gets view-only access of the doc. Questions are submitted via the Zoom chat.<br>* Could use a slack channel and threaded responses.<br> * GitLab Issues|
| People were locked out of the slides. | Share a PDF of the slides in #companyannouncements before the call, so that people who are locked out can still follow along. |
| Because this was a last minute idea that we executed on, we were only able to coordinate for one time. | We should have this at least twice, once in EMEA friendly time zones and one in APAC friendly time zones. Earlier scheduling will make this possible. |
| There were a lot of acronyms and concepts where it would be helpful to have a primer ahead of time so I could track better — things like ARR or TAM come to mind. | Make sure all acronyms are defined upfront. |
| The slide presenter dance | The "next slide please" dance? Something else? |
| Kickoff felt a bit like an extended GC and not visionary enough | Tailor content more appropriately to inspirational/visionary, especially at the beginning |


