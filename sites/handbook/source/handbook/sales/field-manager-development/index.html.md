---
layout: handbook-page-toc
title: "Field Manager Development Program"
description: "The Field Manager Development Program will equip managers with a foundational set of skills & practices for effectively managing remote teams across GitLab’s field organization"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Manager effectiveness is important to GitLab’s continued ability to attract, develop, and retain key talent and deliver scalable, efficient growth. In response, the Corporate L&D, People, and Field Enablement teams are collaborating to launch this FY22 program to equip managers with a foundational set of skills & practices for effectively managing remote teams across GitLab’s field organization.

## Target Audience
All people managers in the GitLab field organization

## Approach
Managers will participate in a series of quarterly training & reinforcement exercises throughout FY22. While there will be formal training elements, a large focus will be on social learning (learning from others) and practical, real-world application to convert knowledge to action.
- **Q1FY22** (2021-03-22): Program Launch & Recruiting Top Talent (virtual)
- **Q2FY22** (2021-05-24, 2021-05-26, and 2021-06-02): Field Manager Challenge (virtual)
- **Q3FY22** (2021-09-18): Field Manager Summit (in-person at Contribute) ([see the FAQs here](https://web.cvent.com/event/aa9c9e95-4dda-4eea-91ec-a1ebecf42e8b/websitePage:8147b15f-0e1c-4daa-93dc-bb71dc089b09))

## Topics
This program will leverage content from the [GitLab Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) and address multiple learning objectives supporting the following categories:
1. Winning Top Talent
1. What is a High Performing Team?
1. Organizational / Team Health
1. Giving Feedback & Coaching
1. Management Operating Rhythm

## Content

### Program Overview
- [Field Manager Development Program Overview](https://youtu.be/oNwqsJV_OmU) (11.5 minutes) ([slides](https://docs.google.com/presentation/d/1O2QSZefPxLf_0T6AQO3NWte73z2X357V1PxVV9wQZX4/edit?usp=sharing)) (GitLab internal only)

### Winning Top Talent
- [The Importance of Winning Top Talent](https://youtu.be/VftKycprJms) (13 minutes, GitLab internal only)
- [Defining Team Needs](https://youtu.be/3BPjETac8ps) (6 minutes, GitLab internal only)
- [Kicking Off the Process With Recruiting](https://youtu.be/6iIVmhJqfUk) (8.5 minutes, GitLab internal only)
- [Importance of the Candidate Experience](https://youtu.be/2hFdo8ndXgs) (15 minutes, GitLab internal only)
- [Inclusive Interview Practices](https://youtu.be/bZN-7yBFoDI) (32 minutes, GitLab internal only)
- [Assessing Candidates](https://youtu.be/OZMqIdrnaEg) (10 minutes, GitLab internal only)

### What is a High Performing Team
- Please complete Module 1 of the [Q2 Field Manager Development Program pre-work journey](https://gitlab.edcast.com/journey/field-manager-development-program-journey) before the session on 2021-05-24
- Building High Performing Teams ([slides](https://docs.google.com/presentation/d/1xEgyPMxNFL7VU5jyGb_-tVykCSNNjpsSh1of6osBq5U/edit?usp=sharing)) (GitLab internal only)

<iframe width="560" height="315" src="https://www.youtube.com/embed/gB5Yfz_MaqI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/hf8dOLeRvQQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Organizational and Team Health
- Please complete Module 2 of the [Q2 Field Manager Development Program pre-work journey](https://gitlab.edcast.com/journey/field-manager-development-program-journey) before the session on 2021-05-26

<iframe width="560" height="315" src="https://www.youtube.com/embed/OM2iujpeKeQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/GCxct4CR-To" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Giving Feedback and Coaching
- Please complete Module 3 of the [Q2 Field Manager Development Program pre-work journey](https://gitlab.edcast.com/journey/field-manager-development-program-journey) before the session on 2021-06-02

<iframe width="560" height="315" src="https://www.youtube.com/embed/P5YuiwNQ87Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/0YaXSbLqork" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Management Operating Rhythm
- Coming later this year

## Core Team
- [Carolyn Bednarz](/company/team/#cbednarz) (People Business Partner, Sales)
- [Josh Zimmerman](/company/team/#Josh_Zimmerman) (Learning & Development Manager)
- [David Somers](/company/team/#dcsomers) (Sr. Director, Field Enablement)
- Field Manager Advisors
    - [Dave Astor](/company/team/#disastor) (Manager, Solutions Architects, US East)
    - [Robbie Byrne](/company/team/#RobbieB) (Area Sales Manager, EMEA)
    - [Ed Cepulis](/company/team/#ecepulis) (Sr. Director, Channel Programs and Enablement)
    - [Michael Leutz](/company/team/#mrleutz) (Manager, Technical Account Managers, EMEA)
    - [Helen Mason](/company/team/#hmason) (Area Sales Manager (SMB), EMEA)
    - [Jacob Potter](/company/team/#jpotter1) (Area Sales Manager, Mid Market First Order)

