---
layout: handbook-page-toc
title: "GitLab CI/CD Hands On Guide- Lab 3"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab CI/CD training course."
---
# GitLab CI/CD Hands On Guide- Lab 3
{:.no_toc}

## LAB 3- CREATE A BASIC CI CONFIGURATION

1. In the GitLab Demo Cloud, locate your CICD Demo project from Lab 2 and open it.
2. Click on your **.gitlab-ci.yml** file to open it.
3. Navigate to the sample project in your other tab - locate the [**ci-starter**] (https://ilt.gitlabtraining.cloud/professional-services-classes/gitlab-ci-cd/gitlab-cicd-hands-on-demo/-/snippets) code snippet, once you have the snippet open, click the **Copy File Contents** icon in the upper right corner of the file.  
4. Return to your CICD Demo project in your other tab; replace the code in your yml file with the ci-starter snippet. 
Note: You should now have a basic CI configuration with only test and build stages.  
5. In the Commit Message field, type **“CI starter”** and click the **Commit Changes** button. 
6. Validate that the configuration is valid and that the pipeline is running by hovering over the blue icon in the upper right corner of the file.  
7. Click on the **pipeline icon** boxed above the file contents to review the Pipeline Graph for that CI configuration.  

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab CI/CD- please submit your changes via Merge Request!
