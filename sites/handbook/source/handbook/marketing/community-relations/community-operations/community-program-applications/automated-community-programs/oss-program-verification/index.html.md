---
layout: handbook-page-toc
title: "GitLab for Open Source Program Verification"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
This page contains details regarding the verification process for the [GitLab for Open Source](/solutions/open-source/) program. GitLab uses a third party, [SheerId](https://www.sheerid.com/), for verification services. 

Please see the [Community Programs Automation Workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/#phase-1-verification) for the full application process. 

## Open Source Program Requirements and Verification
SheerID is not able to provide full automation of the verification phase for the GitLab for Open Source program. This means that there is still some manual verification required by the GitLab Open Source Program Manager and any application processing team available (currently one Community Relations team contractor). GitLab plans to explore ways to more fully automate the application process of the Open Source program in the future.

The most up-to-date requirements for the GitLab for Open Source program are listed on the [OSS program's applicatoin page](/solutions/open-source/join/#requirements).

While unable to fully automate the verification phase for OSS, SheerID is able to make the reviewing process easier by doing the following: 
 * Requesting screenshots to verify the project's publicly visible status and OSI-approved license
 * Asking the applicant to check a box to certify they are not seeking profit 

Verification for the OSS program will require an application processing team until further automation is implemented. 

# Forms
SheerID hosts a specific form for the GitLab for Open Source program.

This form contains the following fields:

- First Name
- Last Name
- Email Address
- Country (drop down list)*
- Institution Name (drop down list)
- Open Source Organization or Project Name
- Project Description
- Publicly Visible Project on GitLab
- OSI Approved License
- Not Seeking Profile (checkbox)
- Marketing email opt-in (checkbox)

(*) Note: US Embargoed countries are not on the list. SheerID is not able to separate out the two different regions in Ukraine so we have asked them to remove Ukraine entirely from the list of countries. If someone from Ukraine does want to apply, they will need to contact us directly and we will determine if they qualify based on the region of origin.

##Legal text on form
The following text was added to the bottom of the form with help from GitLab's legal team.

`By submitting this form, you understand that your information will be shared with GitLab by SheerID and used for verification purposes.`

`For more information please see the [GitLab's Privacy Policy](/privacy/).`

`If you are accepted into the GitLab for Open Source program, you will be subject to [GitLab's terms and conditions](/handbook/legal/opensource-agreement/). All use of GitLab products must comply with United States export control and economic sanction laws.`

# Verification flow

Applicants will have 3 attempts to upload a document providing verification of eligibility for the GitLab for Open Source program. Upon the third unsuccessful attempt, applicant will be rejected (see rejection email here)!FIXME.

The GitLab review team will manually review these documents through the [SheerID Review Portal](https://about.gitlab.com/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/#sheerid-review-portal).

If applicants are successfully verified, they will recieve a welcome email with a coupon code and instructions for next steps. Please see the rest of the [Community Applications Workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/automated-community-programs/#coupon-codes) for more details on what follows.

If the GitLab review team is unable to determine the eligibility based on the screenshots that the applicant provided via the SheerID Review Portal, the GitLab review team will send a rejection email. The applicant can then choose to reach out to opensource@gitlab.com if they believe the rejection was sent in error, or if they have additional questions about eligibility.

To view the welcome and rejection emails that applicants recieve, please see the `Verification Simulation` section below.


# Verification limit

Any individual can apply through the form and verify up to 5 times per 365 days. This limit provides room for error and allows a single individual to apply for licenses on behalf of multiple open source projects. 

This limit is determined by GitLab and set in the system by SheerID. It can be changed at any time.

Note: Linux Foundation Projects will need to go through a slightly different process due to the quantity of applications we expect to see and their special requirements, TBD.


# Verification Simulation

The SheerID verification process for the OSS program can be simulated by following the directions in [this issue](https://gitlab.com/gitlab-com/marketing/community-relations/community-operations/community-operations/-/issues/84) FIXME.

Simulating the verification will allow GitLab team members to view the browser and email communications sent to applicants. 
