---
layout: handbook-page-toc
title: "Editing content into types of topics"
description: "View examples of how to transform content into concept, task, reference, and troubleshooting topics."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We often write pages of documentation that contain different kinds of information.
We want to make sure we're telling readers everything they need to know.

Research shows that information is easier to digest if it's presented in
repeatable patterns, so at GitLab we're moving toward using industry-standard
[topic types](https://docs.gitlab.com/ee/development/documentation/structure.html).

Each topic, or section on a page under a heading, should be identifiable as a concept, task, reference, or
troubleshooting topic. This structure helps our users recognize patterns and
makes both searching and scanning more efficient.

The following examples are intended to help you understand how to take
existing information and edit it into topics of specific types.

## Before

The following topic was trying to be all things to all people. It provided information about groups
and where to find them. It reiterated what was visible in the UI.

![An example](example_1.png)

## After

The information is easier to scan if you move it into concepts and tasks.

### Concept

![An example](example_1_after_concept.png)

### Task

![An example](example_1_after_task.png)
