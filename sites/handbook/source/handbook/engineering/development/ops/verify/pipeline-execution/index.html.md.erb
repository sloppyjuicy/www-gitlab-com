---
layout: handbook-page-toc
title: "Verify:Pipeline Execution Group"
description: "The GitLab team page for the Pipeline Execution Group."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/verify/) and is responsible for delivering on the following directions: 

- [Continuous Integration](/direction/verify/continuous_integration/)
- [Continuous Integration Scaling](/direction/verify/continuous_integration_scaling/)
- [Merge Trains](/direction/verify/merge_trains/)

## Mission

The Verify:Pipeline Exection Group is focused on supporting the functionality with respect to
Continuous Integration [use case](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/). A key focus for the PE group is delivering features that achieve the outcome we track in our performance indicator.

This team maps to [Verify](/handbook/product/categories/#verify-stage) devops stage.

## Performance Indicator

<%= partial("handbook/engineering/development/ops/verify/includes/performance_indicators.erb") %>

### Core domain

- Pipeline processing: processes responsible for transitions of pipelines, stages and jobs.
- Rails-Runner communication: jobs queuing, API endpoints and their underlying functionalities related
  to operations performed by and for Runners.
- Job artifacts: storage and management of artifacts is the gateway for many CI/CD features.

Not included in PE group's domain:

- Secrets Management, see the [direction page](https://about.gitlab.com/direction/release/secrets_management/)
- Pipeline Authoring, see the [direction page](https://about.gitlab.com/direction/verify/pipeline_authoring/)
- Compliance in Pipelines, see the [direction page](https://about.gitlab.com/direction/manage/compliance-management/)


## Team Members

The following people are permanent members of the Verify:Pipeline Execution group:

### Backend
<%= direct_team(manager_role: 'Backend Engineering Manager, Verify:Pipeline Execution & Verify:Pipeline Authoring', role_regexp: /Pipeline Execution/) %>

### Frontend
<%= direct_team(manager_role: 'Frontend Engineering Manager, Verify', role_regexp: /Pipeline Execution/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Verify(?!:)|Pipeline Execution/, direct_manager_role: 'Backend Engineering Manager, Verify:Pipeline Execution & Verify:Pipeline Authoring', other_manager_roles: ['Frontend Engineering Manager, Verify']) %>

## Technologies

<%= partial("handbook/engineering/development/ops/verify/includes/technologies.erb") %>

## Useful Links

<%= partial("handbook/engineering/development/ops/verify/includes/useful_links.erb", locals: { group_label: 'group::pipeline execution', group_issues_url: 'https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Apipeline+execution&scope=all', slack_channel: 'g_ci', slack_url: 'https://gitlab.slack.com/archives/CPCJ8CCCX' }) %>

## How We Work

### Planning

<%= partial("handbook/engineering/development/ops/verify/includes/issue_refinement.erb") %>

#### Weighting Issues

<%= partial("handbook/engineering/development/ops/verify/includes/weighting_issues.erb") %>

### Release Plans

<%= partial("handbook/engineering/development/ops/verify/includes/release_plans.erb") %>

### Workflow

We use the [Pipeline Execution Workflow issue board](https://gitlab.com/groups/gitlab-org/-/boards/1372896?milestone_title=%23started) to track what we work on in the current milestone.

Development moves through workflow states in the following order:

 1. `workflow::design` (if applicable)
 1. `workflow::planning breakdown` 
 1. `workflow::ready for development`
 1. `workflow::in dev`
 1. `workflow::blocked` (as necessary)
 1. `workflow::in review`
 1. `workflow::verification`
 1. `workflow::production`
 1. `Closed`

`workflow::planning breakdown` is driven by Product, but is a collaborative effort between Product, UX and Engineering. The steps for **planning breakdown** typically consists of:
 - Product defining or clarifying the problem statement. Product and UX will collaborate on `problem validation` as needed
 - UX providing designs (as necessary)
 - Engineering clarifying the issue description as stated, and refines and weights the issue once Product and UX have provided enough details to do so.

At any point, if an issue becomes blocked, it would be in the `workflow::blocked` status. If there is a blocking issue, it needs to be added to the issue description or linked to the issue with a 'blocked by' relationship.

`workflow::ready for development` means that an issue has been sufficiently [refined and weighted by Engineering](##how-engineering-refines-issues), upon request by Product and UX

`Closed` means that all code changes associated with the issue are fully enabled on gitlab.com. If it is being rolled out behind a feature flag, it means the feature flag is enabled for all users on gitlab.com.

### What is priority to on work for this milestone?

In the Pipeline Execution Group, we use a series of labels to indicate the highest priority issues in the milestone: `VerifyP1` + `group:pipeline execution`. Once all of those issues have been picked up and are in `workflow:in dev` or beyond we have `VerifyP2` and `VerifyP3` to signal issues that will become `VerifyP1`s in the following milestones. These future product priorities will typically be `ready for development` and have designs or be weighed with proposals for implementation. Beyond the product `VerifyPX` priorities the `ready for development column will be stack ranked daily, such that each teammate can pull from the top of the column and know they are picking work that will be meaningful. 

When teammates select issues, they will assign themselves to the issue and also add the milestone that they believe the issue will most likely ship in. This is also a good time to re-evaluate the weight in case the teammate picking the issue was not the teammate who originally weighed the issue. The milestone assigned, does not have to be the same milestone that you are currently starting work in. Aspriationally, we strive to iterate and want to break down the efforts to ship as much value in the milestone for our users as possible. So, if you see a faster path to doing that when you get an issue feel free to raise a comment and change the proposal to deliver more value. 

#### "What do I work on next?"

Each member of the team can choose which issues to work on during a milestone by assigning the issue to themselves.  When the milestone is well underway and we find ourselves looking for work, we default to working **right to left** on the **issue board** by pulling issues in the right-most column. If there is an issue that a team member can help with on the board, they should do so instead of starting new work. This includes conducting code review on issues that the team member may not be assigned to, if they feel that they can add value and help move the issue along to completion.

Specifically, this means our work is prioritized in the following order:
 * Any verification on code that is in `workflow::verification` or `workflow::production`
 * Conducting code reviews on issues that are `workflow::in review`
 * Unblocking anyone in `workflow::blocked` or `workflow::in dev` if applicable
 * Then, lastly, picking from the top of the `workflow::ready for development` for development column

The goal of this process is to reduce the amount of work in progress (WIP) at any given time. Reducing WIP forces us to "Start less, finish more", and it also reduces cycle time. Engineers should keep in mind that the DRI for a merge request is **the author(s)**, to reflect the importance of teamwork without diluting the notion that having a [DRI is encouraged by our values](/handbook/people-group/directly-responsible-individuals/#dris-and-our-values).

#### Weekly Issue Progress Updates

In order to keep our stakeholders informed of work in progress, we provide updates to issues either by updating the issue's **health status** and/or adding an **async issue update**.

##### Issue Health Status

For issues in the current milestone, we use the [Issue Health Status feature](https://docs.gitlab.com/ee/user/project/issues/#health-status) to indicate probability that an issue will ship in the current milestone. This status is updated by the DRI ([directly responsible individual](/handbook/people-group/directly-responsible-individuals/)) as soon as they recognize the probability has changed. If there is no change to the status, a comment to indicate that it has been the status of the issue has been _assessed_ would be helpful. 

The following are definitions of the health status options:

- `On Track` - The issue has no current blockers, and is likely to be completed in the current milestone.
- `Needs Attention` - The issue is still likely to be completed in the current milestone, but there are setbacks or time constraints that could cause the issue to miss the release due dates.
- `At Risk` - The issue is highly **unlikely** to be completed in the current milestone, and will probably miss the release due dates.

Examples of how status updates are added:
1. If the health status changes from `On Track` to `Needs attention` or `At Risk`, we recommend that the DRI add a short comment stating the reason for the change in an [issue status update](#issue-status-updates).
1. If an issue continues to be `On Track`, the DRI could provide a comment to indicate solutions (whatever it may be) continue to be implemented, and it's still _on track_ to be delivered in the same milestone.

##### Issue status updates

When the DRI is actively working on an issue  (workflow status is `workflow::in dev`, `~orkflow::in review` or `workflow::verification` in the current milestone), they will add a comment into the issue with a status update, detailing:
- the updated issue health status
- notes on what was done based on the updated issue health status (especially if not `On Track`)
- anything else the DRI feels is beneficial to reflect the progress

There are several benefits to this approach:

- Team members can better identify what they can do to help the issue move along the board
- Creates an opening for other team members to engage and collaborate if they have ideas
- Leaving a status update is a good prompt to ask questions and start a discussion
- The wider GitLab community can more easily follow along with product development
- A history of the roadblocks the issue encountered is readily available in case of retrospection
- Product and Engineering Managers are more easily able to keep informed of the progress of work

Expectations for DRIs when providing updates for work in progress:

- Status updates are provided once per week, barring special circumstances (e.g. PTO)
- Ideally updates are made at a logical part of a DRI's workflow to minimize disruption, and not necessarily at the same time/day each week
  - Generally when there is a logical time to leave an update, such as a _change in issue health status_, that is the best time to do so
  - Can be used to present some technical findings or information relevant to other stakeholders

##### Tracking Inactive Issues

As a general rule, any issues being actively worked on have one of the following workflow labels:

- `workflow::in dev`
- `workflow::in review`
- `workflow::verification`
- `workflow::production` (upon **closing** the issue)

The Health Status of these issues should be updated to:

1. `Needs Attention` - on the `1st` of the month.
1. `At Risk` - on the `8th` of the month.

EMs are responsible for manually updating the Health Status of [any inactive issues in the milestone](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous%20integration&milestone_title=%23upcoming&not%5Blabel_name%5D%5B%5D=workflow%3A%3Acanary&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+dev&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+review&not%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&not%5Blabel_name%5D%5B%5D=workflow%3A%3Astaging&not%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&page=2&scope=all&state=opened) accordingly.



### Spikes

<%= partial("handbook/engineering/development/ops/verify/includes/spikes.erb") %>

#### Category Labels

The Pipeline Execution group supports the product marketing categories described below:

| Label                 | |  | | |
| ----------------------| -------| ----|------------| ---|
| `Category:Continuous Integration` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AContinuous%20Integration) | [Direction](/direction/verify/continuous_integration/) | [Documentation](https://docs.gitlab.com/ee/ci/) |
| `Category:Merge Trains` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AMerge%20Trains) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=Category%3AMerge%20Trains) | [Direction](/direction/verify/merge_trains/) | [Documentation](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) |

#### Feature Labels

| Label                 | |  | Description |
| ----------------------| -------| ----|------------|
| `api` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=api&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=api&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to API endpoints for CI features. |
| `CI artifacts` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=CI+artifacts&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI+artifacts&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to [CI build artifacts](http://doc.gitlab.com/ce/ci/build_artifacts/README.html). Formerly `~artifacts` |
| `CI permissions` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=CI+permissions&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI+permissions&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to `CI_JOB_TOKEN` and CI authentication |
| `CI minutes` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=CI+minutes&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI+minutes&label_name[]=group%3A%3Acontinuous%20integration) | All issues and MRs related to how we count continuous integration minutes and calculate usage. Formely `~ci minutes` |
| `CI variables` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=CI+variables&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=CI+variables&label_name[]=group%3A%3Acontinuous%20integration) | Relates to functionality surrounding pre-defined and user-defined variables available in the Build environment. Formerly `~ci variables` |
| `merge requests` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=merge+requests&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=merge+requests&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to CI functionality within the Merge Request. |
| `notifications` | [Issues](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=notifications&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name%5B%5D=notifications&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to various forms of notifications related to CI features. |
| `pipeline analytics` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=pipeline+analytics&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=pipeline+analytics&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to CI pipeline statistics and dashboards. |
| `pipeline processing` | [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=pipeline+processing&label_name[]=group%3A%3Acontinuous%20integration) | [MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?label_name%5B%5D=pipeline+processing&label_name[]=group%3A%3Acontinuous%20integration) | Issues related to the execution of pipeline jobs, including DAG, child pipelines, and matrix build jobs |

#### Other Notable Labels

<%= partial("handbook/engineering/development/ops/verify/includes/notable_labels.erb") %>


### Collaboration with UX and Engineering

To create a high-quality product that is functional and useful – Engineering, PM and Product Designer need to work closely together, combine methodologies, and often connect throughout the product development. Product Management and Product Designers aim to work 3 months in advance of Engineering proposals to ensure the problem definition and solution has been adequately validated prior to building. 

Product Designers play a critical role in the product development of user-facing issues. They collaborate with the Engineering and the Product Manager to design the user experience for the features. Once the design solution is proposed, agreed and validated, the Engineering [DRI](/handbook/people-group/directly-responsible-individuals/) is assigned to implement that design and functionality during the milestone for which the issue is planned.

Product Designer, PM, and Engineering use `workflow::design` to discuss possible complexities, challenges, and uncover blockers around the proposed solution. To avoid blocking reviews later in the product development flow, the Product Designer, PM, and Engineering should work collaboratively throughout the feature design and development process to check-in often, so that the UX approval on merge requests is not required.

#### Inclusive Development

Our process of planning and development relies heavily on overcommunication rather than any approval gates or automated notification mechanisms. We adhere to the proactive mindset and responsibility of everyone involved to make sure every step in the process is as transparent as it can be.

For both planning and building this means direct, cross-functional, and other relevant stakeholders are included early into the process. This makes sure everyone is able to contribute to the best of their capabilities at the right time in the process. This can include, but is not limited to, GitLab objects, Slack, meetings, and daily standups.

Some practical examples of this are:
- When you open up or begin working on an epic, issue, or merge request consider if all stakeholders are aware of this or should be updated. If unsure, error on the side of updating rather than keeping silent.
- When making significant progress make sure this is seen by the relevant stakeholders even if you don't require immediate feedback by mentioning them rather than relying on automated email notifications. Make their involvement explicit.

Note: A good practice when only wanting to inform rather than requesting a direct action from the mentioned stakeholders is to put `FYI` directly following the @mention handle.

#### Breaking down issues for iteration

We suggest using the below steps to reach the best results in the shortest time:

* Once, through user research, we have identified a user need and have generated the big idea to meet that need, Product Designer drives the process of moving that idea into a solution prototype involving PM and Engineering Team early and often.
    * The [UX Definition of Done](/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/continuous-integration/index.html#ci-ux-dod-objectives) can be applied in order to better break down design work and give counterparts better insight into which steps in the design workflow need to be completed before the MVC can move to the development phase.
* We take that prototype through a user testing to validate it is solving the original problem.
* When the solution is validated with users - it is time to break the big solution down into smaller issues involving a whole team (PM, Engineers, Product Designer, QA and Technical Writer).
* Engineers, Product Designer, EM and PM are expected to work closely together to find the most technically feasible and smallest feature set to deliver value to early customers and provide feedback for future product development. Check out [iteration strategies](#iteration-strategies) for help.

We aim to design broadly for an epic or full feature at least one milestone ahead of time and then break the big solution into smaller issues to pick up in the next milestones.
Suppose working one milestone ahead to design the big solution is not possible. In that case, Engineering and Product Designer will define the first most technically feasible and smallest feature set ([MVC](/handbook/values/#minimal-viable-change-mvc)) to satisfy early customers that will be implemented in the same milestone.

#### Reviewing Merge Requests

UX should not be seen as a "gate" for reviewing Merge Requests. To avoid blocking reviews and speed up the response time, Product Designer and the Engineering DRI should work collaboratively throughout the feature development process and check-in often, so the UX approval at the stage of reviewing merge request is not required.

Tips to avoid blocking reviews:
- Product Designers should point Engineers to the needed patterns to ensure product consistency and speed up development time. (example: Pajamas components for Frontend engineers, API documentation for Back-end engineers)
- Whenever Engineering DRI submits a Merge Request that is causing changes to UI or user experience - Engineering DRI should tag the appropriate Product Designer for visibility. However, no UX approval on Merge Requests is expected at the review stage (except when the funtionality was not designed by the UX department).

#### Dealing with Community Merge Requests

For more details on how to contribute to GitLab generally, please see our [documentation](https://docs.gitlab.com/ee/development/contributing/). 

#### Aligning on feature development

The Engineering DRI works with the Product Designer throughout the `workflow:in dev` phase to uncover possible problems with the solution early enough that exhibit unexpected behaviour to what was originally agreed upon. If there are changes to be added that weren't agreed upon in the initial issue - a followup issue should be made and the Engineering DRI should work with the Product Manager to schedule that issue in a following milestone. This allows us to focus on [cleanup over signoff](/handbook/values/#cleanup-over-sign-off), iterate quickly on issues with [a low level of shame](/handbook/values/#low-level-of-shame), and still make sure we accomplish what we've agreed upon. We should be careful not to hold off on completing these followup issues so that we don't build up a significant amount of UX debt issues.

If we find that solutions are consistently not matching the agreed upon design, we will hold a retrospective with the DRI, designer, and product manager to discuss where the gaps in communication are so that we can improve. It may be necessary to begin requiring a UX approval for merge requests on certain issues to help the Engineering DRI meet the requirements.

#### Avoiding crunch times between UX and Engineering

* Ideally, the Product Manager works 3 months ahead of time with a Product Designer to validate the problem and work on the solution. See [Validation track](/handbook/product-development-flow/#validation-track) for more details. This allows us to come up with the bigger idea ahead of time, and work further with Engineering to break it down into smaller iterations. Ideally, this should be completed before the implementation milestone starts.
* Product Designer, PM, and Engineering use the [Design phase](/handbook/product-development-flow/#validation-phase-3-design) in the Validation track to talk about complexities and discuss challenges and uncover blockers. Once we are all in agreement - we can have a put a direction on the board - see [Solution Validation phase](/handbook/product-development-flow/#validation-phase-4-solution-validation) for details.
* Engineers and Product Designers should stay in contact and frequently align throughout the [Build track](/handbook/product-development-flow/#build-track) to avoid unplanned changes.

### Technical debt

<%= partial("handbook/engineering/development/ops/verify/includes/technical_debt.erb", locals: { group: 'CI', board_id: '1438885' }) %>

### Risk Mapping

To better understand the risk environment and each risk's causes and consequences, the Pipeline Execution team uses the [Risk Map](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/risk-map) as our risk management tool to prioritise mitigation strategies and increase Quality.

### Retrospectives

<%= partial("handbook/engineering/development/ops/verify/includes/retrospectives.erb") %>

### Team Communication

<%= partial("handbook/engineering/development/ops/verify/includes/team_communication.erb", locals: { slack_channel: 'g_ci', slack_url: 'https://gitlab.slack.com/archives/CPCJ8CCCX', group_label: 'group:pipeline execution', group_issues_url: 'https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Apipeline+execution&scope=all', gitlab_subgroup: 'ci-group' }) %>

## Developer Onboarding

<%= partial("handbook/engineering/development/ops/verify/includes/developer_onboarding.erb") %>
