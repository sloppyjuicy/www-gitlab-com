---
layout: handbook-page-toc
title: "GitLab Partner Bootcamp"
---

# GitLab Partner Bootcamp

GitLab Partner Bootcamp is here to help you get up to speed on implementing GitLab for your customers.

It is designed to be a self paced training where you can learn about and practice most common usecases and scenarios.

Bootcamp material is divided into sections:
1. Introduction to GitLab
2. Using GitLab
3. Implementing GitLab

These sections are independent, so you can take any of them in any order depending on your needs; we recommend starting with the "Using GitLab" and moving on to "Implementing GitLab"

## Introduction to GitLab
GitLab is a single application for the entire DevSecOps lifecycle. To see what GitLab does have a look at this demo video.

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/-_CDU6NFw7U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>  

### GitLab Basics for Developers 
If you're brand new to GitLab, we recommend watching this video to get familiar with the basics of the development workflow
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/7J7Fdh6XSvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


## Using GitLab

### How to start?

1. The Lab exercises are defined as individual issues within [this GitLab project](https://gitlab.com/technical-bootcamp/using-gitlab);
2. This project also includes instructions on how to create your own copy of the project, as well as details of the Coffee Shop project that you will use as part of the Lab exercises.  Please refer to the instructions and carry out the necessary actions to create your Lab environment.
3. Having completed the above, move to the next section on Agile Project Management. This will help you with the knowledge required to then configure your Bootcamp Lab environment


### Agile Project Management

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/bCDmxG4IIXA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

**Now go to your imported Labs project, find the issue called *Lab 0* and assign it to yourself. It contains all steps needed to set up the environment for the future labs. Execute all tasks in the issue.**


If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage, review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/agile/).

### Code Management & Version Control

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/L6YAUSV51-Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/13MmRlHat_-gRntPijyG9uu7nn1fzfCe3/view?usp=sharing)

**Now go to your project and find issue *Lab 1: Merge Request Approvals* and complete the tasks contained there**.

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/).


### Continuous Integration & AutoDevOps

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/q382wEA4wv0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1rGiDua7LXNTR0sx5kpyK7tCRe9eJZp9O/view?usp=sharing)
**Now go to your project and find issue *Lab 2: AutoDevOps* and complete the tasks contained there**.

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/).

### Custom CI Pipelines

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/nVzNFXLucWY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1PELf3Kshj_kfjagnGDl1lYLh3AuMn29Y/view?usp=sharing)
**Now go to your project and find issue *Lab 3: Custom CI Pipelines* and complete the tasks contained there**.


### GitOps

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/FZVW0w8j6BI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1jxbK-wCKmfX1fIIcvy2hY3ycr18-BHGT/view?usp=sharing)
**Now go to your project and find issue *Lab 4: GitOps* and complete the tasks contained there**.

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/).

### DevSecOps

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/cTzt1FqIX40" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1Q8axZ7774CZ0RDLLQ_UuAmVWiqDnWd1t/view?usp=sharing)
**Now go to your project and find issue *Lab 5: Application Security* and complete the tasks contained there**.

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/).

### Value Stream Management

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/Rltxmm3hgMk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1ZQ5Q0KOQ6pJHEN5_7V3BTOPc61ihkM8A/view?usp=sharing)
**Now go to your project and find issue *Lab 6: Measuring and Managing Value Streams* and complete the tasks contained there**.

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/simplify-devops/).

## Implementing GitLab

### How to Start?

1. You will require access to a cloud hosting provider such as Amazon Web Services, Google Cloud Platform or similar, in order to provision virtual machines and kubernetes clusters as part of the lab excercises. 
2. Certain Bootcamp Labs take advantage of paid Enterprise tier features; to get the full experience your instance of GitLab should be licensed to Ultimate
    * [Get a trial here](https://about.gitlab.com/free-trial/). 
3. The Lab exercises are defined as individual issues within [this GitLab project](https://gitlab.com/technical-bootcamp/implementing-gitlab)

### Installation and Architecture

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rsrjRLZxNd8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1z6MmLqhgzFEXECi8_n7Xj9BTVPafkcdo/view?usp=sharing)
**Now go to your project and find the issues *Lab 1: Omnibus Installation* and *Lab 2: Kubernetes Installation* and complete the tasks contained there**.

### Extending and Integrating
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/VjB7JE0a56o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1wx64uec4UXkSM9xAPMsD3hEVaEqQKMy4/view?usp=sharing)
**Now go to your project and find the issues *Lab 3: SSO with SAML Provider* and complete the tasks contained there**.

### Continuous Delivery with Kubernetes
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/RkVRD0z7eT0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1LMrLs-Fi_FhVAQTqaGStr_Mg1UfhsKVK/view?usp=sharing)
**Now go to your project and find the issues *Lab 4: Kubernetes Provisioning and Management* and complete the tasks contained there**.

### Runner Configurations
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/uXw-cAAUHrE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
[Presentation Slide Deck](https://drive.google.com/file/d/1DShnAltAkph_z3BQgO8kgWCRBnElEgKk/view?usp=sharing)
**Now go to your project and find the issues *Lab 5: Runner Configuration* and *Lab 6: Making it all work together* and complete the tasks contained there**.

